package com.maxst.www.recyclerviewtest;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Charles on 8/9/16.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public static final String TAG = MyAdapter.class.getSimpleName();

    public ArrayList<String> paths;
    private DisplayImageOptions options;

    public MyAdapter(Context context) {
        paths = getPathOfAllImages(context);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumb_image, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        StringBuilder sb = new StringBuilder("file://");
        sb.append(paths.get(position));
        ImageLoader.getInstance().displayImage(
                sb.toString(),
                holder.thumnail,
                options
        );
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView thumnail;

        public MyViewHolder(View itemView) {
            super(itemView);
            thumnail = (ImageView) itemView.findViewById(R.id.thumbnail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "getAdapterPosition():" + getAdapterPosition());
                }
            });
        }
    }

    private Bitmap getThumbnail(Context $context, String path) {
        Cursor cursor = $context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.MediaColumns._ID}, MediaStore.MediaColumns.DATA + "=?",
                new String[]{path}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            cursor.close();
            return MediaStore.Images.Thumbnails.getThumbnail($context.getContentResolver(), id, MediaStore.Images.Thumbnails.MINI_KIND, null);
        }

        cursor.close();
        return null;
    }

    private ArrayList<String> getPathOfAllImages(Context context) {
        ArrayList<String> result = new ArrayList<>();
        Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaColumns.DATA, MediaColumns.DISPLAY_NAME};

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, MediaColumns.DATE_ADDED + " desc");
        int columnIndex = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
//        int columnDisplayname = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);

//        int lastIndex;
        while (cursor.moveToNext()) {
            String absolutePathOfImage = cursor.getString(columnIndex);
//            String nameOfFile = cursor.getString(columnDisplayname);
//            lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
//            lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;

            if (!TextUtils.isEmpty(absolutePathOfImage)) {
                result.add(absolutePathOfImage);
            }
        }

        for (String string : result) {
            Log.i(TAG, "|" + string + "|");
        }
        return result;
    }

}
