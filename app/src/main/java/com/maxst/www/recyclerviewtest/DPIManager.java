package com.maxst.www.recyclerviewtest;

import android.app.Activity;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * Created by Charles on 8/9/16.
 */
public class DPIManager {

    public static final String TAG = DPIManager.class.getSimpleName();

    private static DPIManager instance = new DPIManager();


    public static DPIManager getInstance() {
        return instance;
    }

    private DPIManager() {

    }

    public PointF getScreenSize(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        float density = activity.getResources().getDisplayMetrics().density;
        float dpHeight = displayMetrics.heightPixels / density;
        float dpWidth = displayMetrics.widthPixels / density;

        Log.e(TAG, "dpHeight:" + dpHeight + "  dpWidth:" + dpWidth);

        return new PointF(dpWidth, dpHeight);
    }


}
