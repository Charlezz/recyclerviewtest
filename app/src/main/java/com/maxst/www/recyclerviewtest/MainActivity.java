package com.maxst.www.recyclerviewtest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;

public class MainActivity extends AppCompatActivity {

    private MultiChoiceRecyclerView mRecylerView;
    private MyAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecylerView = (MultiChoiceRecyclerView) findViewById(R.id.recyclerView);

        MyMultipleChoiceAdapter adapter = new MyMultipleChoiceAdapter(this);
        mRecylerView.setLayoutManager(new GridLayoutManager(this, (int) (DPIManager.getInstance().getScreenSize(this).x / 100)));
        mRecylerView.setAdapter(adapter);

//        mAdapter = new MyAdapter(this);
//        mRecylerView.setLayoutManager(new GridLayoutManager(this, (int) (DPIManager.getInstance().getScreenSize(this).x / 100)));
//        mRecylerView.setAdapter(mAdapter);


    }
}
