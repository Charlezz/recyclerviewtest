package com.maxst.www.recyclerviewtest;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Charles on 8/19/16.
 */
public class MyMultipleChoiceAdapter extends MultiChoiceAdapter<MyMultipleChoiceAdapter.MyViewHolder> {

    public static final String TAG = MyMultipleChoiceAdapter.class.getSimpleName();

    public ArrayList<String> paths;
    private DisplayImageOptions options;

    public MyMultipleChoiceAdapter(Context context) {
        paths = getPathOfAllImages(context);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.thumb_image, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        StringBuilder sb = new StringBuilder("file://");
        sb.append(paths.get(position));
        ImageLoader.getInstance().displayImage(
                sb.toString(),
                holder.thumnail,
                options
        );
    }

    @Override
    public int getItemCount() {
        return paths.size();
    }

    public static class MyViewHolder extends MultiChoiceRecyclerView.ViewHolder {
        public ImageView thumnail;

        public MyViewHolder(View itemView) {
            super(itemView);
            thumnail = (ImageView) itemView.findViewById(R.id.thumbnail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "getAdapterPosition():" + getAdapterPosition());
                }
            });
        }
    }

    private ArrayList<String> getPathOfAllImages(Context context) {
        ArrayList<String> result = new ArrayList<>();
        Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME};

        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, MediaStore.MediaColumns.DATE_ADDED + " desc");
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        int columnDisplayname = cursor.getColumnIndexOrThrow(MediaColumns.DISPLAY_NAME);

//        int lastIndex;
        while (cursor.moveToNext()) {
            String absolutePathOfImage = cursor.getString(columnIndex);
//            String nameOfFile = cursor.getString(columnDisplayname);
//            lastIndex = absolutePathOfImage.lastIndexOf(nameOfFile);
//            lastIndex = lastIndex >= 0 ? lastIndex : nameOfFile.length() - 1;

            if (!TextUtils.isEmpty(absolutePathOfImage)) {
                result.add(absolutePathOfImage);
            }
        }

        for (String string : result) {
            Log.i(TAG, "|" + string + "|");
        }
        return result;
    }
}
